#!/bin/bash

locat=$(pwd)
typefile="null"
max_min="null"
locatFile="null"
function usage() {
    echo "USAGE: $0 for check file size with minimize and maximaize file"
    echo "  [-l or --location for defind location]"
    echo "  [-e or --extension] for defind file extension"
    echo "  [-s or --stat] for show minimize and maximaize file"
    echo "  [-h or --help]"
    echo "Examples:"
    echo "$0 -l /etc/ -e txt -s"
    echo "$0 --location /etc/ -e txt -s"
    echo "$0 -extension txt --stats"
    echo
    exit 1
}
function location() {
param=$1
locat=$param
cd $param
locatFile="yes"
}

function extension() {
typefile=$1
}
function pattern(){
awk 'BEGIN{sum=0} 
{sum+=$5} 
END {
print "SUM: ",sum/1024 " KB"
print "file: ",NR
}'
}

function extension_txt() {
ls -l | awk '/^-/'| grep .txt | pattern
}

function extension_xml() {
ls -l | awk '/^-/'| grep .xml | pattern
}
function stats(){
max_min="y"
}
function patternMaxMin(){
awk '{
if(NR==1){
	max=$5
	min=$5
	nameMax=$9
	nameMin=$9
}
if($5 > max){
	max=$5
	nameMax=$9
}
if($5 < min){
	min=$5
	nameMin=$9
}
}
END {
	print "Largest file: ",nameMax," ",max/1024 " KB"
	print "Smallest file: ",nameMin," ",min/1024 " KB"
}'
}
function findMaxMin() {
if [ $typefile == "txt" ]; then
ls -l | awk '/^-/'| grep .txt | patternMaxMin

elif [ $typefile == "xml" ]; then
ls -l | awk '/^-/'| grep .xml | patternMaxMin

else	
ls -l | awk '/^-/' | patternMAxmin

fi
}

while [ $# -gt 0 ]
do
    case $1 in

        -h|--help )
            usage
            ;;

        -l|--location )
            location $2
            shift 2
	    ;;
	-e|--extension )
	    extension $2
	    shift 2
	    ;;
	-s|--stats)
	    stats
	    shift
	    ;;
    esac

done
echo "Location: $locat"
if [[ $locatFile == "yes" && $typefile == "null" ]]; then
ls -l | awk '/^-/' | grep '.txt\|.xml'  | pattern
elif [ $typefile == "null" ]; then
ls -l | awk '/^-/' | pattern
elif [ $typefile == "txt" ]; then
	extension_txt
elif [ $typefile == "xml" ]; then
	extension_xml
fi 
if [ $max_min == "y" ]; then
	findMaxMin
fi
